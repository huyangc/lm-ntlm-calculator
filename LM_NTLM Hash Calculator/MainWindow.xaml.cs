﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.InteropServices;


namespace LM_NTLM_Hash_Calculator
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport(@"LM_NTLM Hash Generator.dll", EntryPoint = "LM_Hash_Generator")]
        extern unsafe static byte* LM_Hash_Generator(char* c);
        [DllImport(@"LM_NTLM Hash Generator.dll", EntryPoint = "NTLM_Hash_Generator")]
        extern unsafe static byte* NTLM_Hash_Generator(char* c);

        public MainWindow()
        {
            InitializeComponent();
        }




        private void UserInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            unsafe
            {
                string str = Userinput.Text;
                fixed (char* p = &(str.ToCharArray()[0]))
                {
                    //设置LM Hash文本框的显示内容
                    byte* re_LM = LM_Hash_Generator(p);
                    
                    string restr_LM = "";
                    for(int i = 0;i<16;i++)
                    {
                        // Get the integral value of the character. 
                        int value = Convert.ToInt32(re_LM[i]);
                        // Convert the decimal value to a hexadecimal value in string form. 
                        if (value < 16)
                        {
                            restr_LM = restr_LM + "0" + String.Format("{0:X}", value);
                        }
                        else
                        {
                            restr_LM += String.Format("{0:X}", value);
                        }
                    }
                    LM.Text = restr_LM;

                    //设置NTLM文本框的显示内容
                    byte* re_NTLM = NTLM_Hash_Generator(p);
                    
                    string restr_NTLM = "";
                    for (int i = 0; i < 16;i++ )
                    {
                        // Get the integral value of the character. 
                        int value = Convert.ToInt32(re_NTLM[i]);
                        if (value < 16)
                        {
                            restr_NTLM = restr_NTLM + "0" + String.Format("{0:X}", value);
                        }
                        else
                        {
                            // Convert the decimal value to a hexadecimal value in string form. 
                            restr_NTLM += String.Format("{0:X}", value);
                        }
                    }
                    

                    NTLM.Text = restr_NTLM;
                }
            }
        }
       
    }
}
